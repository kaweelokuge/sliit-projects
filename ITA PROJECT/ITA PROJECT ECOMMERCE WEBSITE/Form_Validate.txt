//Name: Form_Validate.js

function validateForm()
{
var bookid=document.form1.bookid.value;
var isbnno = document.form1.isbnno.value;
var title = document.form1.title.value;
var author = document.form1.author.value;
var publisher = document.form1.publisher.value;
var quantity = document.form1.quantity.value;
var price = document.form1.price.value;


		
if( bookid =="" || isbnno==""|| title =="" || author =="" || publisher ==""|| quantity =="" || price==""   ) 
{
alert("One or more fields empty!");
return false;
}

if ( isNaN(isbnno)||isbnno.length != 13)
{
alert("Incorrect ISBN-No!(13 digits required)");
return false;
}

if(bookid.length != 5 )
{
alert("Incorrect BookID!(2 Characters and 3 Digits required)");
return false;

}

var exp =  /^[a-zA-Z ]*$/;
if (!(author.match(exp))) {
alert("Enter only text for Author!");
return false;
}

if (!(publisher.match(exp))) {
alert("Enter only text for Publisher!");
return false;
}


if (isNaN(quantity)||quantity<=0)
{
alert("Incorrect Quanity(Number of copies)!");
return false;
}

if ( isNaN(price)|| price<0)
{
alert("Incorrect Price!");
return false;
}




}
