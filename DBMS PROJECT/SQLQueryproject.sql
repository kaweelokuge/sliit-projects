------01-Creating Customer Table------

create table customer_3914 (
cid char(4),
fname varchar(20),
lname varchar(20),
DOB date,
address varchar(20),
NIC char(10),
cus_email varchar(20),
usernme varchar(20),
password varchar(20),

constraint pk1_3914 primary key (cid),
);

------Inserting values to Customer Table------


INSERT INTO customer_3914 VALUES('w001','dilum','diaz','1995-04-01','malabe','345678978v','gtd@gmail.com','jikl','ghjkl');
INSERT INTO customer_3914 VALUES('w002','dilum','diaz','1985-09-08','dematagoda','345678945v','gtd@yahoomail.com','jikl','ghjkl');
INSERT INTO customer_3914 VALUES('w003','kilum','dissanayake','1995-11-02','welisara','335678956v','gee@gmail.com','dilshani','dil');
INSERT INTO customer_3914 VALUES('w004','nelum','fernando','1993-12-3','negombo','245678956v','kee@hotmail.com','nelu','cat');
INSERT INTO customer_3914 VALUES('w005','pavindu','pathinayake','1997-4-7','malabe','445678978v','mee@gmail.com','pavi','dogs');
INSERT INTO customer_3914 VALUES('w006','sameera','smith','1990-9-8','borella','545678967v','klj@gmail.com','sam','elephant');
INSERT INTO customer_3914 VALUES('w007','udara','smith','1998-6-3','kaduwela','645678996v','man@ghotmail.com','uda','dolls');
INSERT INTO customer_3914 VALUES('w008','nimal','pinto','1934-8-31','kaduwela','745678956v','stf@gmail.com','nima','shanu');
INSERT INTO customer_3914 VALUES('w009','pumi','jayasuriya','1923-6-4','colombo','845678945v','san@gmail.com','my baby','series');
INSERT INTO customer_3914 VALUES('w010','kamla','rajapaksha','1994-7-5','matara','945678967v','dil@yahoomail.com','kamaa','manula');



------02-Creating Payment Table------

create table payment_3914(
invoice_no varchar(20),
mode varchar(20),
cid char(4)

constraint pk2_3914 primary key (invoice_no),
constraint fk1_3914 foreign key (cid) references customer_3914(cid)


);

------Inserting values to Payment Table------


INSERT INTO payment_3914 VALUES('in0011','credit card','w001');
INSERT INTO payment_3914 VALUES('in0012','master card','w002');
INSERT INTO payment_3914 VALUES('in0013','credit card','w003');
INSERT INTO payment_3914 VALUES('in0014','credit card','w004');
INSERT INTO payment_3914 VALUES('in0015','visa card','w005');
INSERT INTO payment_3914 VALUES('in0016','visa card','w006');
INSERT INTO payment_3914 VALUES('in0017','credit card','w007');
INSERT INTO payment_3914 VALUES('in0018','master card','w008');
INSERT INTO payment_3914 VALUES('in0019','credit card','w009');
INSERT INTO payment_3914 VALUES('in0020','credit card','w010');


------03-Creating Administrator Table------

create table administrator_3914(
aid char(6),
admin_username varchar(20),
admin_pass varchar(20),
fname varchar(20),
lname varchar(20)

constraint pk4_3914 primary key (aid),

);

------Inserting values to Administrator Table------


INSERT INTO administrator_3914 VALUES('aa0001','kalana','pas001','kalani','dahanayake');
INSERT INTO administrator_3914 VALUES('aa0002','salana','pas002','maduwanthi','wickramasinghe');
INSERT INTO administrator_3914 VALUES('aa0003','lalana','pas003','sam','fernando');
INSERT INTO administrator_3914 VALUES('aa0004','dinusha','pas004','jill','dissanayake');
INSERT INTO administrator_3914 VALUES('aa0005','sidath','pas005','sameera','eakanayake');
INSERT INTO administrator_3914 VALUES('aa0006','manik','pas006','saduni','perera');
INSERT INTO administrator_3914 VALUES('aa0007','vimal','pas007','hashanthi','sangakkara');
INSERT INTO administrator_3914 VALUES('aa0008','sunimal','pas008','mayuran','dahanayake');
INSERT INTO administrator_3914 VALUES('aa0009','pamal','pas009','mayuri','samaraweera');
INSERT INTO administrator_3914 VALUES('aa0010','Kumar','pas010','Kumar','jayawardene');



------04-Creating Theater Table------

create table theater_3914(
tid char(5),
t_type varchar(20),
t_name varchar(20),
location varchar(20)

constraint pk6_3914 primary key (tid),


);

------Inserting values to Theatre Table------


INSERT INTO theater_3914 VALUES('th001','luxury','samanala Theater','malabe'); 
INSERT INTO theater_3914 VALUES('th002','normal','sirasa Theater','jaffna');
INSERT INTO theater_3914 VALUES('th003','laxury','ranaraja Theater','malabe');
INSERT INTO theater_3914 VALUES('th004','normal','kaduwela Theater','kaduwela');
INSERT INTO theater_3914 VALUES('th005','laxury','maya Theater','welisara');
INSERT INTO theater_3914 VALUES('th006','laxury','omaya Theater','maharagama');
INSERT INTO theater_3914 VALUES('th007','laxury','sky Theater','malabe');
INSERT INTO theater_3914 VALUES('th008','normal','moonlight Theater','malabe');
INSERT INTO theater_3914 VALUES('th009','laxury','dynamite Theater','welisara');
INSERT INTO theater_3914 VALUES('th010','normal','c4 Theater','galle');


------05-Creating Show Table------

create table show_3914(
show_id char(5),
name varchar(20),
language varchar(15),
st_time char(5),
end_time char(5),
tid char(5),

constraint pk5_3914 primary key (show_id),
constraint fk5_3914 foreign key (tid) references theater_3914(tid),

);

------Inserting values to Show Table----------------------------------


INSERT INTO show_3914 VALUES('sh001','Maaya','sinhala','10.30','1.30','th001');
INSERT INTO show_3914 VALUES('sh002','doom','hindi','10.30','1.30','th002');
INSERT INTO show_3914 VALUES('sh003',' Ran Maaya','sinhala','10.30','1.30','th003');
INSERT INTO show_3914 VALUES('sh004','Maaya','sinhala','12.30','3.30','th004');
INSERT INTO show_3914 VALUES('sh005','samanala arana','sinhala','10.30','1.30','th005');
INSERT INTO show_3914 VALUES('sh006','james bond','english','12.30','3.30','th006');
INSERT INTO show_3914 VALUES('sh007','muunu','tamil','6.30','9.30','th007');
INSERT INTO show_3914 VALUES('sh008','gagini','tamil','6.30','9.30','th008');
INSERT INTO show_3914 VALUES('sh009','samanmalee','sinhala','1.30','4.30','th009');
INSERT INTO show_3914 VALUES('sh010','ahasa hari usai','sinhala','10.30','1.30','th010');


------06-Creating Ticket Table------


create table ticket_3914(
tno char(6),
ticket_type varchar(15),
show_location varchar(20),
price int,
show_date date,
tdate date,
invoice_no varchar(20),
show_id char(5),
aid char(6)

constraint pk3_3914 primary key (tno),
constraint fk2_3914 foreign key (invoice_no) references payment_3914 (invoice_no),
constraint fk3_3914 foreign key (show_id) references show_3914 (show_id),
constraint fk4_3914 foreign key (aid) references administrator_3914 (aid),

);

------Inserting values to Ticket Table------


INSERT INTO ticket_3914 VALUES('TI1000','balcony','galle',1000,'2016/11/12','2016/11/01','in0011','sh001','aa0001');
INSERT INTO ticket_3914 VALUES('TI2000','balcony','mathra',1000,'2016/11/12','2016/10/10','in0012','sh002','aa0002');
INSERT INTO ticket_3914 VALUES('TI3000','front','malabe',500,'2016/11/03','2016/11/01','in0013','sh003','aa0003');
INSERT INTO ticket_3914 VALUES('TI4000','front','kaduwela',500,'2016/11/23','2016/09/30','in0014','sh004','aa0004');
INSERT INTO ticket_3914 VALUES('TI5000','balcony','malabe',1000,'2016/11/05','2016/11/04','in0015','sh005','aa0005');
INSERT INTO ticket_3914 VALUES('TI6000','balcony','kaduwela',1000,'2016/11/09','2016/11/03','in0016','sh006','aa0006');
INSERT INTO ticket_3914 VALUES('TI7000','balcony','malabe',1000,'2016/11/08','2016/11/02','in0017','sh007','aa0007');
INSERT INTO ticket_3914 VALUES('TI8000','box','colombo',2000,'2016/11/07','2016/11/05','in0018','sh008','aa0008');
INSERT INTO ticket_3914 VALUES('TI9000','box','colombo',2000,'2016/11/04','2016/11/01','in0019','sh009','aa0009');
INSERT INTO ticket_3914 VALUES('TI1001','box','galle',2000,'2016/11/04','2016/11/03','in0020','sh010','aa0010');



------07-Creating Seat Table------

create table seat_3914(
seat_no char(4),
row_no char(4),

constraint pk8_3914 primary key (seat_no));


------Inserting values to Seat Table------

INSERT INTO seat_3914 VALUES('s001','r001');
INSERT INTO seat_3914 VALUES('s002','r002');
INSERT INTO seat_3914 VALUES('s003','r003');
INSERT INTO seat_3914 VALUES('s004','r004');
INSERT INTO seat_3914 VALUES('s005','r005');
INSERT INTO seat_3914 VALUES('s006','r006');
INSERT INTO seat_3914 VALUES('s007','r007');
INSERT INTO seat_3914 VALUES('s008','r008');
INSERT INTO seat_3914 VALUES('s009','r009');
INSERT INTO seat_3914 VALUES('s010','r010');
INSERT INTO seat_3914 VALUES('s011','r011');
INSERT INTO seat_3914 VALUES('s012','r012');
INSERT INTO seat_3914 VALUES('s013','r013');
INSERT INTO seat_3914 VALUES('s014','r014');
INSERT INTO seat_3914 VALUES('s015','r015');



------08-Creating Hall Table------

create table hall_3914(
hall_no char(3),
no_of_seats varchar(4),
tid char(5),
seat_no char(4),

constraint pk7_3914 primary key (hall_no),
constraint fk6_3914 foreign key (seat_no) references seat_3914(seat_no),
constraint fk30_3914 foreign key (tid) references theater_3914(tid),

);

------Inserting values to Hall Table------


INSERT INTO hall_3914 VALUES('H01','100','th001','s001');
INSERT INTO hall_3914 VALUES('H02','200','th002','s002');
INSERT INTO hall_3914 VALUES('H03','300','th003','s003');
INSERT INTO hall_3914 VALUES('H04','400','th004','s004');
INSERT INTO hall_3914 VALUES('H05','500','th005','s005');
INSERT INTO hall_3914 VALUES('H06','600','th006','s006');
INSERT INTO hall_3914 VALUES('H07','400','th007','s007');
INSERT INTO hall_3914 VALUES('H08','800','th008','s008');
INSERT INTO hall_3914 VALUES('H09','900','th009','s009');
INSERT INTO hall_3914 VALUES('H10','300','th010','s010');



------09-Creating Customercontact Table------

create table customercontact_3914(
cid char(4),
phone_no varchar(15)

constraint pk9_3914 primary key (cid),
constraint fk7_3914 foreign key (cid) references customer_3914 (cid)


);

------Inserting values to Customercontact Table------



INSERT INTO customercontact_3914 VALUES('W001','071 3733198');
INSERT INTO customercontact_3914 VALUES('W002','071 3235454');
INSERT INTO customercontact_3914 VALUES('W003','071 6733435');
INSERT INTO customercontact_3914 VALUES('W004','071 5733877');
INSERT INTO customercontact_3914 VALUES('W005','077 4733167');
INSERT INTO customercontact_3914 VALUES('W006','077 2733566');
INSERT INTO customercontact_3914 VALUES('W007','071 1797898');
INSERT INTO customercontact_3914 VALUES('W008','079 7686799');
INSERT INTO customercontact_3914 VALUES('W009','078 3754747');
INSERT INTO customercontact_3914 VALUES('W010','077 3733198');



----Display All the Tables----

select* from customer_3914
select* from payment_3914
select* from ticket_3914
select* from administrator_3914
select* from show_3914
select* from theater_3914
select* from hall_3914
select* from seat_3914
select* from customercontact_3914

----Delete All the tables----
/*
DROP TABLE customercontact_3914
DROP TABLE hall_3914
DROP TABLE seat_3914
DROP TABLE ticket_3914
DROP TABLE show_3914
DROP TABLE theater_3914
DROP TABLE payment_3914
DROP TABLE customer_3914
DROP TABLE administrator_3914 
*/
----01-Display names of the shows that would be played in theaters----
SELECT name
FROM show_3914 

-----02-Display customers who use mobitel phone numbersa(starting with '071')
SELECT c.fname, c.lname, p.phone_no
FROM customer_3914 c, customercontact_3914 p
WHERE c.cid=p.cid AND p.phone_no LIKE '071%'

--3----
SELECT *
FROM theater_3914

--12----
SELECT c.fname,c.lname,p.mode
FROM customer_3914 c, payment_3914 p 
WHERE c.cid=p.cid AND mode='credit card'

--11----
SELECT COUNT(*), address
FROM customer_3914
GROUP BY address
ORDER BY COUNT(*)

--13----
SELECT fname, lname
FROM customer_3914
WHERE address=(SELECT address
FROM customer_3914
WHERE cid = 'w007');

--10----
SELECT t.location, s.name, t.t_name
FROM show_3914 s, theater_3914 t
WHERE t.tid=s.tid AND s.show_id='sh002'

--9----
SELECT s.tid, s.name, t.t_name
FROM show_3914 s, theater_3914 t
WHERE t.tid=s.tid AND s.tid='th007'

--8----
SELECT s.tid, s.name, t.t_name
FROM show_3914 s, theater_3914 t
WHERE t.tid=s.tid AND s.tid='th007'

--4----
UPDATE hall_3914
SET no_of_seats=550
WHERE hall_no='H01'

--5---
SELECT fname
FROM customer_3914
WHERE fname LIKE '%m'

--6----
SELECT aid,admin_username
FROM administrator_3914
WHERE admin_username LIKE '[dks]%'

--7----
SELECT tid,t_type,t_name
FROM theater_3914
WHERE t_type='normal' AND location='malabe'

--14----
SELECT tno
FROM ticket_3914
WHERE price < ALL(SELECT price
				  FROM theater_3914
				   WHERE show_location='colombo');

--15----

---2----
SELECT DISTINCT address
FROM customer_3914
WHERE DOB <'1995/01/01'
--15- Display theater ID and theater names whose names are starting from letters k-s-- 
SELECT tid, t_name
FROM theater_3914
WHERE t_name LIKE '[k-s]%'




