//KAVII
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import mycode.DBconnect;

/**
 *
 * @author Kawee
 */
public class Adminlogin extends javax.swing.JFrame {
       
    Connection con=null;
    ResultSet rs =null;
    PreparedStatement pst =null;
    
    
    
    
    
    /**
     * Creates new form Adminlogin
     */
    public Adminlogin() {
        
        //Connect to DB
        con = DBconnect.connect();
        
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        ausernamebox = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        apasswordbox = new javax.swing.JPasswordField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Username");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(161, 90, 70, 18);

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Login");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(161, 137, 38, 18);

        ausernamebox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ausernameboxActionPerformed(evt);
            }
        });
        getContentPane().add(ausernamebox);
        ausernamebox.setBounds(260, 86, 180, 22);

        jLabel3.setFont(new java.awt.Font("Verdana", 0, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Admin Login");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(248, 13, 147, 30);

        jButton1.setText("Login");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1);
        jButton1.setBounds(260, 194, 100, 49);
        getContentPane().add(apasswordbox);
        apasswordbox.setBounds(260, 136, 180, 22);

        jLabel4.setIcon(new javax.swing.ImageIcon("D:\\My Data Folder\\Desktop\\secure.jpg")); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(0, -10, 680, 350);

        setSize(new java.awt.Dimension(697, 385));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

           try {
              String sql="SELECT Username,Password FROM administrator WHERE Username=? AND Password=? ";
              pst = con.prepareStatement(sql);
              pst.setString(1,ausernamebox.getText());
              pst.setString(2,apasswordbox.getText());
              
              rs= pst.executeQuery();
              if(rs.next()){
                  JOptionPane.showMessageDialog(null,"You have Sucessfully Logged-In");
                   registration r1 = new registration();
                   r1.setVisible(true);
                   this.dispose();
              }
              else{
                  JOptionPane.showMessageDialog(null,"Invalid Username or Password!","Access Denied",JOptionPane.ERROR_MESSAGE);
              }
            
        } catch (Exception e) {
             
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void ausernameboxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ausernameboxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ausernameboxActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Adminlogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Adminlogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Adminlogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Adminlogin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Adminlogin().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField apasswordbox;
    private javax.swing.JTextField ausernamebox;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}
